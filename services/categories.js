var categories = [
  
    {
        "title":"Aeropuerto Internacional",
        "geojson_file":"Aeropuerto_Internacional.geojson",
        "marker":"aeropuerto.png"
    },
    {
        "title":"Aeropuerto Nacional",
        "geojson_file":"Aeropuertos_Nacional.geojson",
        "marker":"marker-4.png"
    },
    {
        "title":"Límite Departamental",
        "geojson_file":"Limite_Departamental.geojson",
        "marker":""
    },
   
    {
        "title":"Puertos",
        "geojson_file":"Puertos.geojson",
        "marker":"puertos.png"
    },
  {
        "title":"Proyectos",
        "geojson_file":"Proyectos_GEO.geojson",
        "marker":"marker-2.png"
    },
     
    {
        "title":"Covid",
        "geojson_file":"TB_Covid_Vacunacion.geojson",
        "marker":"vac_covid.png"
    },
    {
        "title":"Cap Provincia",
        "geojson_file":"Cap_Provincia.geojson",
        "marker":"cap_prov.png"
    }

]