$(window).on("load", function() {

    let URL_BASE = 'http://localhost/leaflet/';

    var map = L.map("map");
    var osm = L.tileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png");
        osm.addTo(map);

    map.setView([-9.15, -75.02579414525093], 6);

/*===================================================
              TILE LAYER               
===================================================*/
    // Satelite Layer
    let googleSat = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
    });
    googleSat.addTo(map);

    // Google Map Layer
    let googleStreets = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
    });
    googleStreets.addTo(map);

    let Stamen_Watercolor = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.{ext}', {
        attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        subdomains: 'abcd',
        minZoom: 1,
        maxZoom: 16,
        ext: 'jpg'
    });
    Stamen_Watercolor.addTo(map);
  
/*===================================================
                  LAYER CONTROL               
===================================================*/
    var baseLayers = {
        "Satellite":googleSat,
        "Google Map":googleStreets,
        "OpenStreetMap": osm,
    };

    var overlays = {};
    var controles =  L.control.layers(baseLayers, overlays, {collapsed: false});

    categories.forEach(async (element) => {

        let urlResource = URL_BASE + 'services/' + element.geojson_file,
            iconResource = URL_BASE + 'images/' + element.marker;

        await fetchGetData(urlResource).then((data) => {

            let markersBar = L.markerClusterGroup();        

            
            let overlayCustom = new L.GeoJSON.AJAX(urlResource, {
                onEachFeature: onEachFeature,
                style: {
                        fillColor:'green',
                        fillOpacity: 0.2,
                        color:'#ffffff'
                    },
                    pointToLayer: function (feature, latlng) {
                        return L.marker(latlng, {icon: generateIcon(iconResource)});
                },
            })

            overlayCustom.on('data:loaded', function (xd) {
                markersBar.addLayer(overlayCustom);
            });

            overlayCustom.on('data:process', function () {
                console.log("cargando")
            });

            controles.addOverlay(markersBar,element.title)
            controles.addTo(map);
        });
    });
});

