$(window).on("load", function() {

    

    let URL_BASE = 'http://localhost/leaflet/';

    var overlayCustom;

    var map = L.map("map");
    var osm = L.tileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png");
        osm.addTo(map);

    map.setView([-9.15, -75.02579414525093], 6);

/*===================================================
              TILE LAYER               
===================================================*/
    // Satelite Layer
    let googleSat = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
    });
    googleSat.addTo(map);

    // Google Map Layer
    let googleStreets = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
    });
    googleStreets.addTo(map);

    let Stamen_Watercolor = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.{ext}', {
        attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        subdomains: 'abcd',
        minZoom: 1,
        maxZoom: 16,
        ext: 'jpg'
    });
    Stamen_Watercolor.addTo(map);
  
/*===================================================
                  LAYER CONTROL               
===================================================*/
    var baseLayers = {
        "Satellite":googleSat,
        "Google Map":googleStreets,
        "OpenStreetMap": osm,
    };

    var overlays = {};
    var controles =  L.control.layers(baseLayers, overlays, {collapsed: false});

    
    var mcgLayerSupportGroup = L.markerClusterGroup.layerSupport() ;

    async function fetchGetData(url){
            const response = await fetch(url);
            const data = await response.json();
            return data;
        
    };
   var  markersLenght;


   categorias.forEach(async (element) => {

        let urlResource = URL_BASE + 'assets/' + element.file_geojson;
        let iconResource = URL_BASE + 'images/' + element.marker;

        var iconCustom = L.icon({
            iconUrl: iconResource,
            iconSize: [32, 37],
            iconAnchor: [16, 37],
            popupAnchor: [0, -28]
        });

            
        await fetchGetData(urlResource).then((data) => {
                    
            let trama =  JSON.parse(JSON.stringify(data));

            var markersBar = L.markerClusterGroup();        

            
            overlayCustom = new L.GeoJSON.AJAX("http://localhost/leaflet/assets/Aeropuerto_Internacional.geojson", {
                onEachFeature: onEachFeature,
              style: {
                    fillColor:'red',
                    fillOpacity: 0.5,
                    color:'#ffffff'
                },
                pointToLayer: function (feature, latlng) {
                    return L.marker(latlng, {icon: iconCustom});
                },
            })

            overlayCustom.on('data:loaded', function (xd) {
                console.log(xd)
                markersBar.addLayer(overlayCustom);
                console.log(markersBar);
                //map.addLayer(markersBar);
             
            });
         
            markersLenght  = trama.features.length;

            //map.fitBounds(overlayCustom.getBounds())

            //this is where the magic happens!      
          //  markersBar.checkIn(monumentsLayerGroup);  
            controles.addOverlay(markersBar,element.title)
            controles.addTo(map);
        });
    });
});

function onEachFeature(feature) {
var popupContent;
if(feature.properties && feature.properties.DEPARTAMEN) {            
    popupContent = feature.properties.DEPARTAMEN;
}
if(feature.properties && feature.properties.LABEL) {            
    popupContent = feature.properties.LABEL;
}
if(feature.properties && feature.properties.Provincia) {            
    popupContent = feature.properties.Provincia;
}
if(feature.properties && feature.properties.NOMBRE_DEL) {            
    popupContent = feature.properties.NOMBRE_DEL;
}
if(feature.properties && feature.properties.nombre) {            
    popupContent = feature.properties.nombre;
}

return(popupContent);
}
